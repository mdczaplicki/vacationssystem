namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userwithcomment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Comment", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Comment");
        }
    }
}
