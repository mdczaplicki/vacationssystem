namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class wuut : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "SuperiorId", "dbo.AspNetUsers");
            AddForeignKey("dbo.AspNetUsers", "SuperiorId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "SuperiorId", "dbo.AspNetUsers");
            AddForeignKey("dbo.AspNetUsers", "SuperiorId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
