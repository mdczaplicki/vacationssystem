namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Leavetakedays : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leave", "DaysTaken", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leave", "DaysTaken");
        }
    }
}
