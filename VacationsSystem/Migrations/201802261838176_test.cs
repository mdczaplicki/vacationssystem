namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Leave", "CreatedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leave", "CreatedDate", c => c.DateTime(nullable: false));
        }
    }
}
