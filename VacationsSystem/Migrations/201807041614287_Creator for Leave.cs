namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatorforLeave : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leave", "CreatorId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Leave", "CreatorId");
            AddForeignKey("dbo.Leave", "CreatorId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Leave", "CreatorId", "dbo.AspNetUsers");
            DropIndex("dbo.Leave", new[] { "CreatorId" });
            DropColumn("dbo.Leave", "CreatorId");
        }
    }
}
