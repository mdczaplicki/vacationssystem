namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveCreationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leave", "CraetionDate", c => c.DateTime(nullable: false, defaultValueSql: "GETDATE()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leave", "CraetionDate");
        }
    }
}
