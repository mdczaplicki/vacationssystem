namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserUSTIDnotrequired : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AspNetUsers", new[] { "UstId" });
            AlterColumn("dbo.AspNetUsers", "UstId", c => c.String(maxLength: 100));
            CreateIndex("dbo.AspNetUsers", "UstId", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.AspNetUsers", new[] { "UstId" });
            AlterColumn("dbo.AspNetUsers", "UstId", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.AspNetUsers", "UstId", unique: true);
        }
    }
}
