namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Leavecreationdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leave", "CreationDate", c => c.DateTime(nullable: false, defaultValueSql: "GETDATE()"));
            DropColumn("dbo.Leave", "CraetionDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leave", "CraetionDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Leave", "CreationDate");
        }
    }
}
