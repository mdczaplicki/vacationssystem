namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UservacationdaysandHolidaymodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "VacationDays", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "VacationDays");
        }
    }
}
