namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class leavestate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LeaveState",
                c => new
                    {
                        LeaveStateId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.LeaveStateId);
            
            AddColumn("dbo.Leave", "LeaveStateId", c => c.Int(nullable: true, defaultValue: 0));
            CreateIndex("dbo.Leave", "LeaveStateId");
            AddForeignKey("dbo.Leave", "LeaveStateId", "dbo.LeaveState", "LeaveStateId");
            DropColumn("dbo.Leave", "IsConfirmed");
            DropColumn("dbo.Leave", "IsNotified");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leave", "IsNotified", c => c.Boolean(nullable: false));
            AddColumn("dbo.Leave", "IsConfirmed", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Leave", "LeaveStateId", "dbo.LeaveState");
            DropIndex("dbo.Leave", new[] { "LeaveStateId" });
            DropColumn("dbo.Leave", "LeaveStateId");
            DropTable("dbo.LeaveState");
        }
    }
}
