using System.Data.Entity.Migrations;
using VacationsSystem.DAL;
using VacationsSystem.Extensions;
using VacationsSystem.Models;

namespace VacationsSystem.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<VacationsContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VacationsContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.LeaveType.SeedEnumValues<LeaveType, Leave.LeaveTypeEnum>(@enum => @enum);
            context.LeaveState.SeedEnumValues<LeaveState, Leave.LeaveStateEnum>(@enum => @enum);
            context.SaveChanges();
        }
    }
}
