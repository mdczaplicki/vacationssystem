namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uservacationdaysdelta : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "VacationDaysDelta", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "VacationDaysDelta");
        }
    }
}
