namespace VacationsSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class leaveoptionaluser : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Leave", new[] { "UserId" });
            AlterColumn("dbo.Leave", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Leave", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Leave", new[] { "UserId" });
            AlterColumn("dbo.Leave", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Leave", "UserId");
        }
    }
}
