﻿using System.Web.Optimization;

namespace VacationsSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/new-site.min.css"));

            bundles.Add(new ScriptBundle("~/javascript").Include(
                "~/Scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/fontsawesome").Include(
                "~/Content/fontawesome-all.min.css"));
        }
    }
}
