﻿using System;
using System.Reflection;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.OwinHost;
using Owin;
using VacationsSystem.DAL;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.EmailServices;
using VacationsSystem.Models;

namespace VacationsSystem.App_Start
{
    public class IdentityConfig
    {
        private static readonly Lazy<IKernel> LazyKernel = new Lazy<IKernel>(CreateKernel);
        public static IKernel Kernel => LazyKernel.Value;

        public void Configuration(IAppBuilder app)
        {

            app.UseNinjectMiddleware(() => Kernel);
            Kernel.Bind<IAppBuilder>().ToConstant(app);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home/Login"),
            });
        }

        private static void RegisterService(IKernel kernel)
        {
            kernel.Bind<IdentityDbContext<User>>().To<VacationsContext>().InSingletonScope();
            kernel.Bind<RoleManager<UserRole>>().ToSelf().InRequestScope();
            kernel.Bind<IIdentityMessageService>().To<LeaveEmailService>().InSingletonScope();
            kernel.Bind<IEmailNotificationService>().To<LeaveEmailService>().InRequestScope();
            kernel.Bind<IUserTokenProvider<User, string>>().To<TotpSecurityStampBasedTokenProvider<User, string>>()
                .InRequestScope();
            kernel.Bind<LeaveUserManager>().ToMethod(c => LeaveUserManager.Create(
                kernel.Get<VacationsContext>(), 
                kernel.Get<IIdentityMessageService>(),
                kernel.Get<IUserTokenProvider<User, string>>())).InRequestScope();
            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication)
                .InRequestScope();
//            kernel.Bind<IUserStore<User>>().To<UserStore<User>>().InRequestScope().WithConstructorArgument("context", kernel.Get<VacationsContext>());
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
        }

        private static StandardKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            RegisterService(kernel);
            return kernel;
        }
    }
}