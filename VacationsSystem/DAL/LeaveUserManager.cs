﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using VacationsSystem.Models;

namespace VacationsSystem.DAL
{
    public class LeaveUserManager : UserManager<User>
    {
        private LeaveUserManager(IUserStore<User> store) : base(store)
        {
        }

        public static LeaveUserManager Create(
            IdentityDbContext<User> context, 
            IIdentityMessageService messageService,
            IUserTokenProvider<User, string> tokenProvider)
        {
            var manager = new LeaveUserManager(new UserStore<User>(context))
            {
                EmailService = messageService,
                UserTokenProvider = tokenProvider
            };
            return manager;
        }
    }
}