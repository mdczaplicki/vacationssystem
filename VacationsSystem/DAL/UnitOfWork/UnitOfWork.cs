﻿#region usings

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using VacationsSystem.Common;
using VacationsSystem.DAL.Repositories;
using VacationsSystem.Models;

#endregion

namespace VacationsSystem.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        #region Fields

        private readonly VacationsContext context;

        private IRepository<User> userRepository;

        private IRepository<UserRole> userRoleRepository;

        private IRepository<Leave> leaveRepository;
        
        #endregion

        #region Constructors

        public UnitOfWork()
        {
            this.context = new VacationsContext();
        }

        #endregion

        #region Properties

        public IRepository<User> UserRepository =>
            this.userRepository ?? (this.userRepository = new Repository<User>(this.context));

        public IRepository<UserRole> UserRoleRepository =>
            this.userRoleRepository ?? (this.userRoleRepository = new Repository<UserRole>(this.context));

        public IRepository<Leave> LeaveRepository =>
            this.leaveRepository ?? (this.leaveRepository = new Repository<Leave>(this.context));

        #endregion

        #region Private methods

        private void SetEntryToUnchangedIfNoPropertyChanged(ref DbEntityEntry entry)
        {
            if (entry.State != EntityState.Modified)
            {
                return;
            }

            var originalValues = entry.OriginalValues;
            var currentValues = entry.CurrentValues;

            foreach (var propertyName in originalValues.PropertyNames)
            {
                var original = originalValues[propertyName];
                var current = currentValues[propertyName];

                if (!Equals(original, current))
                {
                    return;
                }
            }

            entry.State = EntityState.Unchanged;
        }

        private void LoadSubordinates(User user)
        {
            this.context.Entry(user).Collection(u => u.DirectSubordinates).Load();
            foreach (var userDirectSubordinate in user.DirectSubordinates)
            {
                this.LoadSubordinates(userDirectSubordinate);
            }
        }

        #endregion

        #region Public methods

        public Task<int> Complete()
        {
            return TaskQueue.Enqueue(() => this.context.SaveChangesAsync());
        }

        public bool HasUnsavedChanges(object entity)
        {
            var entry = this.context.Entry(entity);

            this.SetEntryToUnchangedIfNoPropertyChanged(ref entry);

            return entry.State == EntityState.Added || entry.State == EntityState.Modified
                   || entry.State == EntityState.Deleted;
        }

        public void SetModified(object entity)
        {
            var entry = this.context.Entry(entity);
            entry.State = EntityState.Modified;
        }

        public bool IsUnchanged(object entity)
        {
            var entry = this.context.Entry(entity);

            return entry.State == EntityState.Unchanged;
        }

        public bool IsDetached(object entity)
        {
            var entry = this.context.Entry(entity);

            return entry.State == EntityState.Detached;
        }

        public void UndoAll()
        {
            foreach (var entry in this.context.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

        public void UndoEntity(object entity)
        {
            var entry = this.context.Entry(entity);
            switch (entry.State)
            {
                case EntityState.Modified:
                    entry.State = EntityState.Unchanged;
                    break;
                case EntityState.Added:
                    entry.State = EntityState.Detached;
                    break;
                case EntityState.Deleted:
                    entry.Reload();
                    break;
            }
        }

        public void LoadAllSubordinates(ref User user)
        {
            this.LoadSubordinates(user);
        }

        #endregion

        public void Dispose()
        {
            this.context?.Dispose();
        }
    }
}