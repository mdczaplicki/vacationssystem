﻿using System.Threading.Tasks;
using VacationsSystem.DAL.Repositories;
using VacationsSystem.Models;

namespace VacationsSystem.DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<User> UserRepository { get; }

        IRepository<UserRole> UserRoleRepository { get; }

        IRepository<Leave> LeaveRepository { get; }

        #region Public methods

        Task<int> Complete();

        bool HasUnsavedChanges(object entity);

        void SetModified(object entity);

        bool IsUnchanged(object entity);

        bool IsDetached(object entity);

        void UndoAll();

        void UndoEntity(object entity);

        #endregion

        void Dispose();
    }
}