﻿#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Threading.Tasks;
using X.PagedList;

#endregion

namespace VacationsSystem.DAL.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        #region Public methods

        void Add(TEntity entity);

        void AddRange(List<TEntity> entities);
        Task<int> Count();

        Task<int> Count(Expression<Func<TEntity, bool>> filter);

        Task<List<TColumn>> Find<TColumn>(Expression<Func<TEntity, TColumn>> column,
            Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includes);

        Task<List<TEntity>> Find<TOrderBy>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TOrderBy>> order, ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes);

        Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate,
            string order, ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes);

        Task<TEntity> Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

        Task<TEntity> Get(string id);

        Task<TEntity> GetRecursive(string id, Expression<Func<TEntity, object>> recursion,
            Expression<Func<TEntity, object>> recursionInclude);

        Task<TEntity> GetRecursive(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> recursion,
            Expression<Func<TEntity, object>> recursionInclude, params Expression<Func<TEntity, object>>[] includes);

        Task<List<TEntity>> GetAll(params Expression<Func<TEntity, object>>[] includes);

        Task<List<TColumn>> GetAll<TColumn>(Expression<Func<TEntity, TColumn>> column);

        Task<IPagedList<TEntity>> GetPage<TOrderBy>(int elemNum, int startIndex, Expression<Func<TEntity, TOrderBy>> order,
            ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes);

        Task<IPagedList<TEntity>> GetPageWithFilter<TOrderBy>(int elemNum, int startIndex,
            Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TOrderBy>> order,
            ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes);

        Task<IPagedList<TEntity>> GetPageWithFilter(int elemNum, int startIndex,
            Expression<Func<TEntity, bool>> filter, string order,
            ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes);

        void LoadMore(IEnumerable<TEntity> source, params Expression<Func<TEntity, object>>[] includes);

        void Remove(TEntity entity);

        void RemoveRange(List<TEntity> entities);

        #endregion
    }
}