﻿#region usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using VacationsSystem.Common;
using VacationsSystem.Extensions;
using WebGrease.Css.Extensions;
using X.PagedList;

#endregion

namespace VacationsSystem.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Private methods
        
        private void LoadRelatives(TEntity entity, Expression<Func<TEntity, object>> recursion, Expression<Func<TEntity, object>> recursionInclude)
        {
            this.Context.Entry(entity).LoadReferenceOrCollection(recursion);
            if (recursionInclude != null)
            {
                this.Context.Entry(entity).LoadReferenceOrCollection(recursionInclude);
            }

            var invoked = recursion.Compile().Invoke(entity);
            var type = invoked?.GetType();
            if (type?.GetInterface(nameof(ICollection)) != null)
            {
                foreach (var child in (ICollection<TEntity>) invoked)
                {
                    this.LoadRelatives(child, recursion, recursionInclude);
                }
            }
            else if (invoked != null)
            {
                this.LoadRelatives((TEntity) invoked, recursion, recursionInclude);
            }

        }

        #endregion

        #region Public methods

        public void Add(TEntity entity)
        {
            this.DbSet.Add(entity);
        }

        public void AddRange(List<TEntity> entities)
        {
            this.DbSet.AddRange(entities);
        }

        public Task<int> Count()
        {
            return TaskQueue.Enqueue(() => this.DbSet.CountAsync());
        }

        public Task<int> Count(Expression<Func<TEntity, bool>> filter)
        {
            return TaskQueue.Enqueue(() => this.DbSet.Where(filter ?? (i => true)).CountAsync());
        }

        public Task<List<TColumn>> Find<TColumn>(Expression<Func<TEntity, TColumn>> column,
            Expression<Func<TEntity, bool>> predicate)
        {
            return TaskQueue.Enqueue(() => this.DbSet.Where(predicate ?? (i => true)).Select(column).ToListAsync());
        }

        public Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () => this.DbSet.IncludeMultiple(includes).Where(predicate ?? (i => true)).ToListAsync());
        }

        public Task<List<TEntity>> Find<TOrderBy>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TOrderBy>> order, ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () =>
                        this.DbSet.IncludeMultiple(includes)
                            .Where(predicate ?? (i => true))
                            .Order(order, direction)
                            .ToListAsync());
        }

        public Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate, string order, ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () =>
                        this.DbSet.IncludeMultiple(includes)
                            .Where(predicate ?? (i => true))
                            .Order(order, direction)
                            .ToListAsync());
        }

        public Task<TEntity> Get(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () => this.DbSet.IncludeMultiple(includes).Where(predicate ?? (i => true)).FirstOrDefaultAsync());
        }

        public Task<TEntity> Get(string id)
        {
            return TaskQueue.Enqueue(() => this.DbSet.FindAsync(id));
        }

        public Task<TEntity> GetRecursive(string id, Expression<Func<TEntity, object>> recursion,
            Expression<Func<TEntity, object>> recursionInclude)
        {
            return TaskQueue.Enqueue(async () =>
            {
                var entity = await this.DbSet.FindAsync(id);
                if (entity == null)
                {
                    return null;
                }
                this.LoadRelatives(entity, recursion, recursionInclude);
                return entity;
            });
        }

        public Task<TEntity> GetRecursive(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> recursion,
            Expression<Func<TEntity, object>> recursionInclude, params Expression<Func<TEntity, object>>[] includes)
        {
            return TaskQueue.Enqueue(async () =>
            {
                var entity = await this.DbSet.IncludeMultiple(includes).Where(predicate ?? (i => true)).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return null;
                }
                this.LoadRelatives(entity, recursion, recursionInclude);
                return entity;
            });
        }

        public Task<List<TEntity>> GetAll(params Expression<Func<TEntity, object>>[] includes)
        {
            return TaskQueue.Enqueue(() => this.DbSet.IncludeMultiple(includes).ToListAsync());
        }

        public Task<List<TColumn>> GetAll<TColumn>(Expression<Func<TEntity, TColumn>> column)
        {
            return TaskQueue.Enqueue(() => this.DbSet.Select(column).ToListAsync());
        }

        public Task<IPagedList<TEntity>> GetPage<TOrderBy>(int elemNum, int startIndex,
            Expression<Func<TEntity, TOrderBy>> order, ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () =>
                        this.DbSet.IncludeMultiple(includes)
                            .Order(order, direction)
                            .ToPagedListAsync(startIndex, elemNum));
        }

        public Task<IPagedList<TEntity>> GetPageWithFilter(int elemNum, int startIndex,
            Expression<Func<TEntity, bool>> filter, string order,
            ListSortDirection direction = ListSortDirection.Ascending,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () =>
                        this.DbSet.Where(filter ?? (i => true))
                            .IncludeMultiple(includes)
                            .Order(order, direction)
                            .ToPagedListAsync(startIndex, elemNum));
        }

        public void LoadMore(IEnumerable<TEntity> source, params Expression<Func<TEntity, object>>[] includes)
        {
            source.ForEach(e => includes.ForEach(i => this.Context.Entry(e).Reference(i).Load()));
        }

        public Task<IPagedList<TEntity>> GetPageWithFilter<TOrderBy>(int elemNum, int startIndex,
                Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TOrderBy>> order,
                ListSortDirection direction = ListSortDirection.Ascending,
                params Expression<Func<TEntity, object>>[] includes)
        {
            return
                TaskQueue.Enqueue(
                    () =>
                        this.DbSet.Where(filter ?? (i => true))
                            .IncludeMultiple(includes)
                            .Order(order, direction)
                            .ToPagedListAsync(startIndex, elemNum));
        }

        public void Remove(TEntity entity)
        {
            this.DbSet.Remove(entity);
        }

        public void RemoveRange(List<TEntity> entities)
        {
            this.DbSet.RemoveRange(entities);
        }

        #endregion

        protected readonly VacationsContext Context;

        protected readonly DbSet<TEntity> DbSet;

        public Repository(VacationsContext context)
        {
            this.Context = context;
            this.DbSet = this.Context.Set<TEntity>();
        }
    }
}