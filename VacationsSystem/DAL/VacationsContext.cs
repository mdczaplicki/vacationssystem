﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using VacationsSystem.Models;

namespace VacationsSystem.DAL
{
    public class VacationsContext : IdentityDbContext<User>
    {
        #region Contructors

        public VacationsContext() : base("name=VacationsSystemDBEntities")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        #endregion

        #region Properties

        public virtual DbSet<Leave> Leave { get; set; }
        public virtual DbSet<LeaveType> LeaveType { get; set; }
        public virtual DbSet<LeaveState> LeaveState { get; set; }
        public virtual DbSet<UserRole> IdentityRole { get; set; }

        #endregion

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<User>().HasOptional(u => u.Superior).WithMany(u => u.DirectSubordinates).HasForeignKey(u => u.SuperiorId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Leave>().HasOptional(l => l.User).WithMany(u => u.Leaves).HasForeignKey(l => l.UserId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Leave>().HasRequired(l => l.LeaveType).WithMany(lt => lt.Leaves).HasForeignKey(l => l.LeaveTypeId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Leave>().HasRequired(l => l.LeaveState).WithMany(ls => ls.Leaves).HasForeignKey(l => l.LeaveStateId).WillCascadeOnDelete(false);
        }

        #endregion

    }
}