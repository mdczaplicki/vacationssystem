﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VacationsSystem.Extensions
{
    public class CloseDateRangeAttribute : RangeAttribute
    {
        public CloseDateRangeAttribute() : base(
            typeof(DateTime),
            DateTime.Now.AddMonths(-1).UnifiedFormat(),
            DateTime.Now.AddYears(1).UnifiedFormat())
        { }
    }
}