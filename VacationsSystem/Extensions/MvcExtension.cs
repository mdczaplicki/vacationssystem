﻿using System.Web.Mvc;
using JetBrains.Annotations;

namespace VacationsSystem.Extensions
{
    public static class MvcExtension
    {
        public static MvcHtmlString RenderIf(this MvcHtmlString value, bool render, string falseVal = "")
        {
            return render ? value : new MvcHtmlString(falseVal);
        }

        public static string IsActive(this HtmlHelper source, [AspMvcController] string controller = null, [AspMvcAction] string action = null)
        {
            const string cssClass = "active";
            var currentAction = (string) source.ViewContext.RouteData.Values["action"];
            var currentController = (string) source.ViewContext.RouteData.Values["controller"];

            if (controller == null)
            {
                controller = currentController;
            }
            if (action == null)
            {
                action = currentAction;
            }
            return controller == currentController && action == currentAction ? cssClass : string.Empty;
        }
    }
}