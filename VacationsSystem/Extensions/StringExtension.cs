﻿using System;
using System.Text.RegularExpressions;

namespace VacationsSystem.Extensions
{
    public static class StringExtension
    {
        public static bool Contains(this string source, string value, StringComparison comparisonType)
        {
            return source.IndexOf(value, comparisonType) >= 0;
        }

        public static bool IsUstEmail(this string source)
        {
            return Regex.Match(source, @"ust-global[.]com$", RegexOptions.IgnoreCase).Success;
        }

        public static bool IsHkEmail(this string source)
        {
            return Regex.Match(source, @"hk-finance[.]pl$", RegexOptions.IgnoreCase).Success;
        }
    }
}