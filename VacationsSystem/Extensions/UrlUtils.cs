﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;

namespace VacationsSystem.Extensions
{
    public class UrlUtils
    {
        public static string AppendRouteValues(string url, NameValueCollection parameters)
        {
            if (parameters == null)
            {
                return url;
            }

            var query = !url.Contains("?")
                ? new NameValueCollection()
                : HttpUtility.ParseQueryString(url.Split(new[] {'?'}, 2)[1]);

            var elements = new List<string>();
            foreach (string key in parameters)
            {
                if (query.AllKeys.Any(k => k.Equals(key, StringComparison.InvariantCultureIgnoreCase)))
                {
                    continue;
                }
                parameters[key].Split(',').ForEach(value => elements.Add($"{key}={value}"));
            }
            if (elements.Count > 0)
            {
                return url + (url.Contains("?") ? "&" : "?") + string.Join("&", elements);
            }

            return url;
        }
    }
}