﻿using System;
using System.Linq;
using Nager.Date;
using Nager.Date.Extensions;

namespace VacationsSystem.Extensions
{
    public class DateTools
    {
        public static int HowManyDays(DateTime start, DateTime end)
        {

            if (start > end)
            {
                return 0;
            }
            var holidays = DateSystem.GetPublicHoliday(CountryCode.PL, start, end).Select(h => h.Date);

            var span = end - start;
            var businessDays = span.Days + 1;
            var fullWeekCount = businessDays / 7;
            if (businessDays > fullWeekCount * 7)
            {
                var firstDayOfWeek = start.DayOfWeek == DayOfWeek.Sunday
                    ? 7 : (int)start.DayOfWeek;
                var lastDayOfWeek = end.DayOfWeek == DayOfWeek.Sunday
                    ? 7 : (int)end.DayOfWeek;
                if (lastDayOfWeek < firstDayOfWeek)
                {
                    lastDayOfWeek += 7;
                }
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)
                    {
                        businessDays -= 2;
                    }
                    else if (lastDayOfWeek >= 6)
                    {
                        businessDays -= 1;
                    }
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)
                {
                    businessDays -= 1;
                }
            }

            businessDays -= fullWeekCount + fullWeekCount;
            foreach (var holiday in holidays)
            {
                if (start <= holiday && holiday <= end && !holiday.IsWeekend(CountryCode.PL))
                {
                    --businessDays;
                }
            }

            return businessDays;
        }
    }
}