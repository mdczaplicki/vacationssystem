﻿using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace VacationsSystem.Extensions
{
    public static class QueryableExtension
    {
        public static IOrderedQueryable<T> Order<T, TKey>(this IQueryable<T> source, Expression<Func<T, TKey>> order,
            ListSortDirection direction = ListSortDirection.Ascending)
        {
            return ListSortDirection.Ascending == direction ? source.OrderBy(order) : source.OrderByDescending(order);
        }

        public static IOrderedQueryable<T> Order<T>(this IQueryable<T> source, string propertyName,
            ListSortDirection direction = ListSortDirection.Ascending)
        {
            if (propertyName.Contains("date", StringComparison.CurrentCultureIgnoreCase))
            {
                return ListSortDirection.Ascending == direction
                    ? source.OrderBy(ToLambda<T, DateTime>(propertyName))
                    : source.OrderByDescending(ToLambda<T, DateTime>(propertyName));

            }
            return ListSortDirection.Ascending == direction
                ? source.OrderBy(ToLambda<T, object>(propertyName))
                : source.OrderByDescending(ToLambda<T, object>(propertyName));
        }

        private static Expression<Func<T, A>> ToLambda<T, A>(string propertyName)
        {
            var propertyNames = propertyName.Split('.');
            var parameter = Expression.Parameter(typeof(T));
            var body = propertyNames.Aggregate<string, Expression>(parameter, Expression.Property);

            if (typeof(A) == typeof(object))
            {
                return Expression.Lambda<Func<T, A>>(body, parameter);
            }
            return Expression.Lambda<Func<T, A>>(Expression.Convert(body, typeof(A)), parameter);
        }

        public static IQueryable<T> IncludeMultiple<T>(this IQueryable<T> query,
            params Expression<Func<T, object>>[] includes) where T : class
        {
            if (includes == null)
            {
                return query;
            }

            query = includes.Aggregate(query, (current, include) => current.Include(include));
            return query;
        }
    }
}