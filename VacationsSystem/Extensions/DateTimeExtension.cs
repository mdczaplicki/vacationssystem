﻿using System;
using Nager.Date;
using Nager.Date.Extensions;

namespace VacationsSystem.Extensions
{
    public static class DateTimeExtension
    {
        public static void IterateDay(this DateTime source, out DateTime ret)
        {
            ret = source.AddDays(1);
        }

        public static string GetTypeForExcel(this DateTime source)
        {
            return DateSystem.IsPublicHoliday(source, CountryCode.PL) ? "S" :
                source.IsWeekend(CountryCode.PL) ? "W" : "P";
        }

        public static string UnifiedFormat(this DateTime source)
        {
            return source.ToString("yyyy-MM-dd");
        }
    }
}