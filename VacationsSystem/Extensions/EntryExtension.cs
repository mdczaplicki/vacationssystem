﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using WebGrease.Css.Extensions;

namespace VacationsSystem.Extensions
{
    public static class EntryExtension
    {
        public static void ReferenceMultiple<T>(this DbEntityEntry<T> source,
            params Expression<Func<T, object>>[] includes) where T : class
        {
            includes?.ForEach(source.LoadReferenceOrCollection);
        }
    }
}