﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;

namespace VacationsSystem.Extensions
{
    public static class DbEntityEntryExtension
    {
        public static void LoadReferenceOrCollection<TEntity>(this DbEntityEntry<TEntity> source,
            Expression<Func<TEntity, object>> reference) where TEntity : class
        {
            var type = reference.Compile().Invoke(source.Entity)?.GetType() ?? typeof(TEntity);
            if (type.GetInterface(nameof(ICollection)) != null)
            {
                var converted = Expression.Convert(reference.Body, typeof(ICollection<object>));
                source.Collection(
                    Expression.Lambda<Func<TEntity, ICollection<object>>>(converted, reference.Parameters)).Load();
            }
            else
            {
                source.Reference(reference).Load();
            }
        }
    }
}