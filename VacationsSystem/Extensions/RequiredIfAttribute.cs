﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace VacationsSystem.Extensions
{
    public class RequiredIfUstEmailAttribute : RequiredAttribute
    {
        private string PropertyName { get; set; }
        private readonly RequiredAttribute innerAttribute;

        public RequiredIfUstEmailAttribute(string propertyName)
        {
            this.PropertyName = propertyName;
            this.innerAttribute = new RequiredAttribute();
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var dependentValue = context.ObjectInstance.GetType().GetProperty(this.PropertyName)?.GetValue(context.ObjectInstance, null);

            if (dependentValue?.GetType() == typeof(string))
            {
                var val = (string) dependentValue;
                if (val.IsUstEmail())
                {
                    if (!this.innerAttribute.IsValid(value))
                    {
                        return new ValidationResult(this.FormatErrorMessage(context.DisplayName), new[] { context.MemberName });
                    }
                }
            }

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessageString,
                ValidationType = "requiredif",
            };
            rule.ValidationParameters["dependentproperty"] = (context as ViewContext)?.ViewData.TemplateInfo.GetFullHtmlFieldId(this.PropertyName);

            yield return rule;
        }
    }
}