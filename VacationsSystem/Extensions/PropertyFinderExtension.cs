﻿using System;
using System.Linq;

namespace VacationsSystem.Extensions
{
    public static class PropertyFinderExtension
    {
        public static bool HasProperty(this Type source, string propertyName)
        {
            var nestType = source;
            foreach (var propName in propertyName.Split('.'))
            {
                var property = nestType.GetProperties().FirstOrDefault(p =>
                    string.Equals(p.Name, propName, StringComparison.CurrentCultureIgnoreCase));
                if (property == null)
                {
                    return false;
                }
                nestType = property.PropertyType;
            }
            return true;
        }
    }
}