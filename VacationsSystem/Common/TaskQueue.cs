﻿using System;
using System.Threading.Tasks;

namespace VacationsSystem.Common
{
    public static class TaskQueue
    {
        #region Fields

        private static Task previous = Task.FromResult(false);

        private static readonly object key = new object();

        #endregion

        #region Public methods

        public static Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            lock (key)
            {
                var next = previous.ContinueWith(t => taskGenerator()).Unwrap();
                previous = next;
                return next;
            }
        }

        public static Task Enqueue(Func<Task> taskGenerator)
        {
            lock (key)
            {
                var next = previous.ContinueWith(t => taskGenerator()).Unwrap();
                previous = next;
                return next;
            }
        }

        #endregion
    }
}