﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Ninject.Infrastructure.Language;
using OfficeOpenXml;
using RandomColorGenerator;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Extensions;
using VacationsSystem.Models;
using VacationsSystem.ViewModels;

namespace VacationsSystem.Services
{
    public class LeaveService
    {
        private readonly IUnitOfWork unitOfWork;

        public LeaveService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets a list of Leaves prepared for Calendar. List is based on option.
        /// Subordinates uese a list of all subordinates (recursively) of specified user.
        /// Superiors uses a list of all superiors (recursively) of specified user.
        /// Coworkeres uses a list of users that share the same superior as specified user.
        /// Self uses a specified user.
        /// </summary>
        /// <param name="inUsers">Specified list of users</param>
        /// <param name="option"></param>
        /// <returns></returns>
        public async Task<List<CalendarLeaveViewModel>> GetLeavesForCaledar(List<User> inUsers, Enums.CalendarOptionEnum option)
        {
            IEnumerable<Leave> leaves;
            var userToColor = new Dictionary<string, string>();
            var inUsersIds = inUsers.Select(i => i.Id);
            if (option == Enums.CalendarOptionEnum.Subordinates)
            {
                var recursiveUser = await this.unitOfWork.UserRepository.GetRecursive(
                    u => inUsersIds.Contains(u.Id),
                    u => u.DirectSubordinates,
                    u => u.Leaves,
                    u => u.Leaves);
                var users = recursiveUser.AllSubordinates.ToList();
                leaves = users.SelectMany(u => u.Leaves).ToList();
                var colors = RandomColor.GetColors(ColorScheme.Random, Luminosity.Light, users.Count);
                userToColor = users.Select(u => u.Id).Zip(colors, (k, v) => new { k, v })
                    .ToDictionary(x => x.k, x => ColorTranslator.ToHtml(x.v));
            }
            else if (option == Enums.CalendarOptionEnum.Superiors)
            {
                var recursiveUser = await this.unitOfWork.UserRepository.GetRecursive(
                    u => inUsersIds.Contains(u.Id),
                    u => u.Superior,
                    u => u.Leaves);
                var users = recursiveUser.AllSuperiors.ToList();
                leaves = users.SelectMany(u => u.Leaves).ToList();
                var colors = RandomColor.GetColors(ColorScheme.Random, Luminosity.Light, users.Count);
                userToColor = users.Select(u => u.Id).Zip(colors, (k, v) => new { k, v })
                    .ToDictionary(x => x.k, x => ColorTranslator.ToHtml(x.v));
            }
            else if (option == Enums.CalendarOptionEnum.Coworkers)
            {
                var inUsersSuperios = inUsers.Select(i => i.SuperiorId);
                var users = await this.unitOfWork.UserRepository.Find(
                    u => inUsersSuperios.Contains(u.SuperiorId) && !inUsersIds.Contains(u.Id),
                    includes: u => u.Leaves);
                leaves = users.SelectMany(u => u.Leaves).ToList();
                var colors = RandomColor.GetColors(ColorScheme.Random, Luminosity.Light, users.Count);
                userToColor = users.Select(u => u.Id).Zip(colors, (k, v) => new { k, v })
                    .ToDictionary(x => x.k, x => ColorTranslator.ToHtml(x.v));
            }
            else
            {
                leaves = await this.unitOfWork.LeaveRepository.Find(
                    l => inUsersIds.Contains(l.UserId),
                    l => l.LeaveType,
                    l => l.User);
                foreach (var inUser in inUsers)
                {
                    userToColor.Add(inUser.Id, ColorTranslator.ToHtml(RandomColor.GetColor(ColorScheme.Random, Luminosity.Light)));
                }
            }
            this.unitOfWork.LeaveRepository.LoadMore(leaves, l => l.LeaveType);
            leaves = leaves.Where(l => (l.LeaveStateId &
                 ((int) Leave.LeaveStateEnum.Approved | (int) Leave.LeaveStateEnum.ApprovedAndNotified)) != 0);
            var leavesVm = leaves.Select(l => new CalendarLeaveViewModel(l, userToColor));
            return leavesVm.ToList();
        }

        /// <summary>
        /// Gets a list of leaves for all subordinates of specified user.
        /// Leaves are filtered based on their state.
        /// Type of the leaves is also included in the returned list.
        /// Returned list is ordered from the newest to the oldest.
        /// </summary>
        /// <param name="userId">Specified user</param>
        /// <param name="filterState"></param>
        /// <param name="filterUser"></param>
        /// <param name="filterStartDate"></param>
        /// <param name="filterEndDate"></param>
        /// <param name="orderDirection"></param>
        /// <param name="page"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public async Task<X.PagedList.IPagedList<Leave>> GetLeavesOfAllSubordinatesAndSelfIncludingType(
            string userId,
            int filterState,
            string filterUser,
            DateTime filterStartDate,
            DateTime filterEndDate,
            string sortOrder,
            ListSortDirection orderDirection,
            int page)
        {
            var user = await this.unitOfWork.UserRepository.GetRecursive(userId,
                u => u.DirectSubordinates, null);
            var users = user.AllSubordinates.Concat(new[] {user})
                .Where(u => u.FullName.Contains(filterUser, StringComparison.OrdinalIgnoreCase)).Select(u => u.Id);

            Expression<Func<Leave, bool>> findExpression = l 
                => users.Contains(l.UserId)
                && (DbFunctions.TruncateTime(l.StartDate) > filterStartDate || DbFunctions.TruncateTime(l.EndDate) > filterStartDate)
                && (DbFunctions.TruncateTime(l.StartDate) < filterEndDate || DbFunctions.TruncateTime(l.EndDate) < filterEndDate)
                && (l.LeaveStateId != null && (l.LeaveStateId.Value & filterState) != 0);


            return await this.unitOfWork.LeaveRepository.GetPageWithFilter(10, page, findExpression, sortOrder, orderDirection,
                l => l.LeaveType);
        }

        public async Task<X.PagedList.IPagedList<Leave>> GetFilteredListOfLeaves(string filterUser, DateTime filterStartDate,
            DateTime filterEndDate, int filterType, int filterState, int page, string sortOrder, ListSortDirection orderDirection, bool employed=true)
        {
            Expression<Func<Leave, bool>> findExpression = l =>
                (l.User.LastName + " " + l.User.FirstName).Contains(filterUser)
                && (DbFunctions.TruncateTime(l.StartDate) > filterStartDate || DbFunctions.TruncateTime(l.EndDate) > filterStartDate)
                && (DbFunctions.TruncateTime(l.StartDate) < filterEndDate || DbFunctions.TruncateTime(l.EndDate) < filterEndDate)
                && (l.LeaveTypeId != null && (l.LeaveTypeId.Value & filterType) != 0)
                && (l.LeaveStateId != null && (l.LeaveStateId.Value & filterState) != 0)
                && (l.User.IsEmployed == employed);

            return await this.unitOfWork.LeaveRepository.GetPageWithFilter(10, page, findExpression, sortOrder,
                orderDirection, l => l.User, l => l.LeaveType, l => l.LeaveState);
        }

        public async Task<ExcelPackage> GetExcelPackage(string filterUser, DateTime filterStartDate,
            DateTime filterEndDate, int filterType, int filterState, bool? filterEmployed = null)
        {
            Expression<Func<Leave, bool>> findExpression = l =>
                (l.User.LastName + " " + l.User.FirstName).Contains(filterUser)
                && (DbFunctions.TruncateTime(l.StartDate) > filterStartDate || DbFunctions.TruncateTime(l.EndDate) > filterStartDate)
                && (DbFunctions.TruncateTime(l.StartDate) < filterEndDate || DbFunctions.TruncateTime(l.EndDate) < filterEndDate)
                && (l.LeaveTypeId != null && (l.LeaveTypeId.Value & filterType) != 0)
                && (l.LeaveStateId != null && (l.LeaveStateId.Value & filterState) != 0)
                && (filterEmployed == null || l.User.IsEmployed == filterEmployed);
            var leaves = await this.unitOfWork.LeaveRepository.Find(findExpression, l => l.User, l => l.LeaveType);

            var package = new ExcelPackage();
            var worksheet = package.Workbook.Worksheets.Add("Urlopy");
            worksheet.Cells["A1"].Value = "Nazwisko";
            worksheet.Cells["B1"].Value = "Imie";
            worksheet.Cells["C1"].Value = "Nazwa_do_importu";
            worksheet.Cells["D1"].Value = "Nazwa_zrodlowa";
            worksheet.Cells["E1"].Value = "Data_od";
            worksheet.Cells["F1"].Value = "Data_do";

            var i = 2;

            foreach (var leave in leaves)
            {
                if (leave.StartDate == null || leave.EndDate == null)
                {
                    continue;
                }
                worksheet.Cells[i, 1].Value = leave.User?.LastName;
                worksheet.Cells[i, 2].Value = leave.User?.FirstName;
                worksheet.Cells[i, 3].Value = leave.GetTypeName();
                worksheet.Cells[i, 4].Value = leave.GetTypeName();
                worksheet.Cells[i, 5].Value = leave.StartDate.Value.UnifiedFormat();
                worksheet.Cells[i, 6].Value = leave.EndDate.Value.UnifiedFormat();
                ++i;
            }

            return package;
        }
    }
}