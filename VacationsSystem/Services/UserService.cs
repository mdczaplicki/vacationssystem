﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Extensions;
using VacationsSystem.Models;

namespace VacationsSystem.Services
{
    public class UserService : IDisposable
    {
        private readonly IUnitOfWork unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets a list of all subordinates of specified user and adds specified user to this list.
        /// </summary>
        /// <param name="userId">Specified user</param>
        /// <returns></returns>
        public async Task<List<User>> GetAllSubordinatesAndSelf(string userId)
        {
            var recursiveLoggedUser = await this.unitOfWork.UserRepository.GetRecursive(userId, u => u.DirectSubordinates, null);
            var userList = recursiveLoggedUser.AllSubordinates.Where(u => u.EmailConfirmed && u.IsEmployed).ToList();
            userList.Add(recursiveLoggedUser);
            return userList;
        }

        public async Task<X.PagedList.IPagedList<User>> GetFilteredListOfUsers(string filterName, string filterUstId,
            string filterSuperior, bool? filterIsEmployed, int page, string sortOrder, ListSortDirection orderDirection)
        {
            Expression<Func<User, bool>> findExpression = u =>
                (u.LastName + " " + u.FirstName).Contains(filterName)
                && u.UstId.Contains(filterUstId)
                && (u.Superior == null || (u.Superior.LastName + " " + u.Superior.FirstName).Contains(filterSuperior))
                && (filterIsEmployed == null || u.IsEmployed == filterIsEmployed);

            return await this.unitOfWork.UserRepository.GetPageWithFilter(10, page, findExpression, sortOrder,
                orderDirection, u => u.Superior);
        }

        /// <summary>
        /// Gets a UserRole for new User. If it doesn't exists - create it and return.
        /// </summary>
        /// <param name="user">User for which to get UserRole</param>
        /// <returns></returns>
        public async Task<UserRole> GetRoleForNewUser(User user)
        {
            var roleName = user.Email.IsUstEmail() ? "User"
                           : (user.Email.IsHkEmail() ? "Hr"
                           : string.Empty);
            var userRole = (await this.unitOfWork.UserRoleRepository.Find(ur => ur.Name == roleName)).FirstOrDefault();
            if (userRole == null)
            {
                userRole = new UserRole(roleName);

                this.unitOfWork.UserRoleRepository.Add(userRole);
                await this.unitOfWork.Complete();
            }

            return userRole;
        }

        public void Dispose()
        {
            this.unitOfWork?.Dispose();
        }
    }
}