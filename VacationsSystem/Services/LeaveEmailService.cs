﻿using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using VacationsSystem.EmailServices;

namespace VacationsSystem.App_Start
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class LeaveEmailService : IIdentityMessageService, IEmailNotificationService
    {
        private readonly SmtpClient client = new SmtpClient();

        public LeaveEmailService()
        {
            this.client.DeliveryMethod = SmtpDeliveryMethod.Network;
        }

        public Task SendAsync(IdentityMessage message)
        {
            return this.SendAsync(new MailMessage
            {
                From = new MailAddress(ConfigurationManager.AppSettings["emailAddress"]),
                To = { new MailAddress(message.Destination)},
                ReplyToList = { ConfigurationManager.AppSettings["replyToList"] },
                Subject = message.Subject,
                Body = message.Body,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
                IsBodyHtml = true
            });
        }

        public Task SendNotification(string userEmail, string approverEmail, string subject, string body)
        {
            return this.SendAsync(new MailMessage
            {
                From = new MailAddress(ConfigurationManager.AppSettings["emailAddress"]),
                To = {ConfigurationManager.AppSettings["hrEmail"], userEmail, approverEmail},
                ReplyToList = {ConfigurationManager.AppSettings["hrEmail"], userEmail, approverEmail},
                Subject = subject,
                Body = body,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
                IsBodyHtml = true
            });
        }

        private Task SendAsync(MailMessage message)
        {
            message.Body = "Hello,<br/><br/>" + message.Body +
                           "<br/><br/>If you have any issues, please reply to this message.";
            return this.client.SendMailAsync(message);
        }
    }
}