﻿using System.Threading.Tasks;

namespace VacationsSystem.EmailServices
{
    public interface IEmailNotificationService
    {
        Task SendNotification(string userEmail, string approverEmail, string subject, string body);
    }
}
