﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using VacationsSystem.Extensions;
using VacationsSystem.Models;

namespace VacationsSystem.ViewModels
{
    public class AddLeaveViewModel
    {
        public AddLeaveViewModel() { }

        public AddLeaveViewModel(Leave leave)
        {
            this.LeaveTypeId = leave.LeaveTypeId ?? 0;
            this.StartDate = leave.StartDate.Value;
            this.EndDate = leave.EndDate.Value;
            this.Description = leave.Description;
            this.DaysTaken = leave.DaysTaken;
            this.LeaveId = leave.Id;
        }

        public string LeaveId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Leave type")]
        public int LeaveTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Start date")]
        [CloseDateRange(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "WrongDate")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "End date")]
        [Remote("AreDatesOk", "JsonCheck", AdditionalFields = "StartDate", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "EndDateLater")]
        [CloseDateRange(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "WrongDate")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Days taken")]
        [Remote("EnoughDays", "JsonCheck", AdditionalFields = "StartDate", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "NotEnoughDays")]
        public int? DaysTaken { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(1000, ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "TooLong")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "User")]
        public string UserId { get; set; }
    }
}