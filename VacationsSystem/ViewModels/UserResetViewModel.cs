﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using VacationsSystem.Attributes;

namespace VacationsSystem.ViewModels
{
    public class UserResetViewModel
    {
        public string UserId { get; set; }
        public string Token { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [EmailAddress]
        [Display(Name = "UST email")]
        [CustomEmail(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "WrongEmailDomain")]
        [Remote("IsEmailInUse", "JsonCheck", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "EmailDoesntExist")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "PasswordIsDifferent")]
        public string ConfirmPassword { get; set; }
    }
}