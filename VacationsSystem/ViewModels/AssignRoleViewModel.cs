﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VacationsSystem.ViewModels
{
    public class AssignRoleViewModel
    {
        [Display(Name = "Roles")]
        public IEnumerable<string> SelectedRoles { get; set; }

        [Display(Name = "User")]
        public string FullName { get; set; }

        [Display(Name = "Available roles")]
        public IEnumerable<string> AvailableRoles { get; set; }

        public string[] InitialRoles { get; set; }

        public string Id { get; set; }
    }
}