﻿using System.ComponentModel.DataAnnotations;
using VacationsSystem.Models;

namespace VacationsSystem.ViewModels
{
    public class UserDetailsViewModel
    {
        public UserDetailsViewModel() {}

        public UserDetailsViewModel(User user)
        {
            this.UserId = user.Id;
            this.FullName = user.FullName;
            this.VacationDays = user.VacationDays;
            this.VacationDaysDelta = user.VacationDaysDelta;
            this.Comment = user.Comment;
        }

        public string UserId { get; set; }

        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Display(Name = "Vacation days")]
        public int? VacationDays { get; set; }

        [Display(Name = "Increment days by")]
        public int VacationDaysDelta { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }
    }
}