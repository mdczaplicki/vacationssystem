﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using VacationsSystem.Attributes;
using VacationsSystem.Extensions;

namespace VacationsSystem.ViewModels
{
    public class UserRegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "User name *")]
        [StringLength(50, MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "WrongLength")]
        [Index(IsUnique = true)]
        [Remote("IsUserFree", "JsonCheck", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "UserNameAlreadyExists")]
        [RegularExpression(@"^(?![a-zA-Z]\d{5}).*$", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "UstIdAsUserName")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "First name *")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "TooLong")]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Last name *")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "TooLong")]
        public string LastName { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [EmailAddress]
        [Display(Name = "UST email *")]
        [Remote("IsUserFree", "JsonCheck", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "EmailAlreadyExists")]
        [CustomEmail(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "WrongEmailDomain")]
        [RegularExpression(@"^(?![a-zA-Z]\d{5}).*$", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "UstIdInEmail")]
        public string Email { get; set; }
        
        [RequiredIfUstEmail("Email", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "TooLong")]
        [Display(Name = "UST ID *")]
        [Remote("IsUserFree", "JsonCheck", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "UstIdAlreadyExists")]
        [RegularExpression(@"^[a-zA-Z]\d{5}$", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "InvalidUstId")]
        public string UstId { get; set; }

        [EmailAddress]
        [Display(Name = "Private email")]
        public string PrivateEmail { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Password *")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Confirm password *")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "PasswordIsDifferent")]
        public string ConfirmPassword { get; set; }

    }
}