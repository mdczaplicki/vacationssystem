﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace VacationsSystem.ViewModels
{
    public class UserForgotViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [EmailAddress]
        [Display(Name = "UST email")]
        [Remote("IsEmailInUse", "JsonCheck", HttpMethod = "POST", ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "EmailDoesntExist")]
        public string Email { get; set; }
    }
}