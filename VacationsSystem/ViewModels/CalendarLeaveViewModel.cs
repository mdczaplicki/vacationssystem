﻿using System.Collections.Generic;
using Newtonsoft.Json;
using VacationsSystem.Extensions;
using VacationsSystem.Models;

namespace VacationsSystem.ViewModels
{
    public class CalendarLeaveViewModel
    {
        public CalendarLeaveViewModel(Leave leave, IDictionary<string, string> userToColor)
        {
            this.Id = leave.Id;
            this.Title = leave.User?.FullName + " - " + leave.LeaveType?.Name;
            this.OtherTitle = leave.User?.FullName + " - " + leave.StartDate?.ToShortDateString() + " - " + leave.EndDate?.UnifiedFormat();
            this.StartDate = leave.StartDate?.ToString("yyyy-MM-dd");
            this.EndDate = leave.EndDate?.AddDays(1).ToString("yyyy-MM-dd");
            this.Color = userToColor[leave.UserId ?? ""];
        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("otherTitle")]
        public string OtherTitle { get; set; }

        [JsonProperty("start")]
        public string StartDate { get; set; }

        [JsonProperty("end")]
        public string EndDate { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("borderColor")]
        public readonly string BorderColor = "#CECECE";
    }
}