﻿using System.ComponentModel.DataAnnotations;

namespace VacationsSystem.ViewModels
{
    public class UserLoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Username or UST ID")]
        [StringLength(50, MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "WrongLength")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Error), ErrorMessageResourceName = "IsRequired")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}