﻿/// <binding AfterBuild='concat, cssmin, concat:dist' />
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({
        concat: {
            css: {
                src: [
                    'Scripts/lib/bootstrap/dist/css/bootstrap.css',
                    'Scripts/lib/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.css',
                    'Scripts/lib/bootstrap-select/dist/css/bootstrap-select.css',
                    'Scripts/lib/fullcalendar/dist/fullcalendar.css',
                    'Content/site.css'
                ],
                dest: 'Content/new-site.css'
            },
            dist: {
                src: [
                    'Scripts/lib/jquery/dist/jquery.min.js',
                    'Scripts/lib/jquery-ui/jquery-ui.min.js',
                    'Scripts/lib/moment/min/moment.min.js',
                    'Scripts/lib/popper.js/dist/umd/popper.min.js',
                    'Scripts/leave/alertHide.js',
                    'Scripts/lib/bootstrap/dist/js/bootstrap.min.js',
                    'Scripts/lib/respond/dest/respond.min.js',
                    'Scripts/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'Scripts/lib/bootstrap-select/dist/js/bootstrap-select.min.js',
                    'Scripts/lib/fullcalendar/dist/fullcalendar.min.js',
                    'Scripts/lib/fullcalendar/dist/fullcalendar.print.min.js',
                    'Scripts/lib/jquery-validation/dist/jquery.validate.min.js',
                    'Scripts/lib/jquery-validation/dist/additional-methods.min.js',
                    'Scripts/lib/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js',
                    'Scripts/lib/moment/min/locales.min.js',
                ],
                dest: 'Scripts/site.js'
            }
        },

        cssmin: {
            css: {
                src: 'Content/new-site.css',
                dest: 'Content/new-site.min.css'
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', []);

    // Build task(s).
    grunt.registerTask('build:css', ['concat:css', 'cssmin:css']);
};