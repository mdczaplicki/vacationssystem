﻿namespace VacationsSystem.Enums
{
    public enum CalendarOptionEnum
    {
        Self,
        Superiors,
        Coworkers,
        Subordinates
    }
}