﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace VacationsSystem.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CustomEmailAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            if (value == null || value.GetType() != typeof(string))
            {
                return false;
            }
            var val = (string) value;
            return Regex.Match(val, @"(ust-global[.]com|hk-finance[.]pl)$", RegexOptions.IgnoreCase).Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessageString,
                ValidationType = "customemail"
            };
        }
    }
}