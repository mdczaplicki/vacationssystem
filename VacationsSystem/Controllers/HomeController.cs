﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using VacationsSystem.Attributes;
using VacationsSystem.DAL;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Enums;
using VacationsSystem.Models;
using VacationsSystem.Services;
using VacationsSystem.ViewModels;
using X.PagedList;

namespace VacationsSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserService userService;
        private readonly LeaveService leaveService;

        private readonly LeaveUserManager leaveUserManager;
        private readonly IAuthenticationManager authenticationManager;
        private readonly IUnitOfWork unitOfWork;

        public HomeController(LeaveUserManager userManager, IAuthenticationManager authManager, IUnitOfWork unitOfWork)
        {
            this.leaveUserManager = userManager;
            this.authenticationManager = authManager;
            this.unitOfWork = unitOfWork;
            this.userService = new UserService(unitOfWork);
            this.leaveService = new LeaveService(unitOfWork);
        }

        #region HttpGet

        public ActionResult Index()
        {
            return this.View();
        }

        [AuthorizeRedirect(Roles = "User")]
        public async Task<ActionResult> UserInfo()
        {
            var userId = this.User.Identity.GetUserId();
            var user = await this.unitOfWork.UserRepository.GetRecursive(userId, u => u.Superior, null);
            this.ViewBag.Approvers = user.AllSuperiors.Where(u => this.leaveUserManager.IsInRole(u.Id, "Approver"))
                .Select(u => u.FullName);
            return this.View(user);
        }

        public ActionResult About()
        {
            this.ViewBag.Message = "Your application description page.";

            return this.View();
        }

        public ActionResult Contact()
        {
            this.ViewBag.Message = "Your contact page.";

            return this.View();
        }

        public ActionResult Login()
        {
            return this.View();
        }

        public ActionResult Register()
        {
            return this.View();
        }

        public ActionResult Logout()
        {
            this.authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            this.TempData["Message"] = "Logged out";
            return this.RedirectToAction("Index");
        }

        [AuthorizeRedirect(Roles = "User")]
        public async Task<ActionResult> Calendar(string[] userId, Enums.CalendarOptionEnum? calendarOption)
        {
            userId = userId ?? new [] { this.User.Identity.GetUserId() };
            var selectedUsers = (await this.unitOfWork.UserRepository.Find(u => userId.Contains(u.Id)));
            if (selectedUsers == null || selectedUsers.Count == 0)
            {
                return this.HttpNotFound();
            }
            if (selectedUsers.Count > 1)
            {
                calendarOption = CalendarOptionEnum.Self;
                this.ViewBag.DisableGroup = true;
            }
            else
            {
                calendarOption = calendarOption ?? CalendarOptionEnum.Self;
            }

            var leaves = await this.leaveService.GetLeavesForCaledar(selectedUsers, calendarOption.Value);
            var availableUsers = await (await this.userService.GetAllSubordinatesAndSelf(this.User.Identity.GetUserId())).OrderBy(u => u.LastName).ToListAsync();

            this.ViewBag.UserSelectList = new MultiSelectList(availableUsers, "Id", "FullName", selectedUsers.Select(u => u.Id));
            this.ViewBag.CalendarOption = calendarOption;
            return this.View(leaves);
        }

        public async Task<ActionResult> ConfirmEmail(string userId, string token)
        {
            var result = await this.leaveUserManager.ConfirmEmailAsync(userId, token);
            if (result != IdentityResult.Success)
            {
                return this.RedirectToAction("EmailConfirm", "Error", new { errors = string.Join(", ", result.Errors) });
            }
            this.TempData["SuccessMessage"] = "Email confirmed";
            return this.View("Login");
        }

        public ActionResult ForgotPassword()
        {
            return this.View();
        }

        public async Task<ActionResult> ResetPassword(string userId, string token)
        {
            var user = await this.unitOfWork.UserRepository.Get(userId);
            var model = new UserResetViewModel
            {
                Email = user.Email,
                UserId = userId,
                Token = token
            };

            return this.View(model);
        }

        #endregion

        #region HttpPost

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult >Login(UserLoginViewModel model, string returnUrl)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View();
            }

            var user = (await this.unitOfWork.UserRepository.Find(u =>
                u.UserName == model.Login || u.UstId == model.Login)).FirstOrDefault();
            if (user == null)
            {
                this.ModelState.AddModelError("Login", Resources.Error.UserDoesntExist);
                return this.View();
            }
            
            if (!await this.leaveUserManager.CheckPasswordAsync(user, model.Password))
            {
                this.ModelState.AddModelError("Password", Resources.Error.WrongPassword);
                return this.View();
            }

            if (!await this.leaveUserManager.IsEmailConfirmedAsync(user.Id))
            {
                this.ModelState.AddModelError(string.Empty, Resources.Error.EmailNotConfirmed);
                return this.View();
            }

            var ident = this.leaveUserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            this.authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);

            this.TempData["SuccessMessage"] = "Logged in";
            if (!string.IsNullOrEmpty(returnUrl) && this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            return this.RedirectToAction("UserInfo");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Register(UserRegisterViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View();
            }
            var user = new User(model);
            var result = await this.leaveUserManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                result.Errors.ForEach(e => this.ModelState.AddModelError(string.Empty, e));
                return this.View();
            }

            var userRole = await this.userService.GetRoleForNewUser(user);
            await this.leaveUserManager.AddToRoleAsync(user.Id, userRole.Name);

            var token = await this.leaveUserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            token = HttpUtility.UrlEncode(token);
            var callbackUrl = this.Url.Action("ConfirmEmail", "Home", new { userId = user.Id, token }, protocol: this.Request.Url?.Scheme);
            await this.leaveUserManager.SendEmailAsync(user.Id, "Confirm email",
                "Please confirm your email by clicking <a href=\"" + callbackUrl + "\">here</a>");

            this.TempData["SuccessMessage"] = "Registered";
            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(UserForgotViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            var user = await this.leaveUserManager.FindByEmailAsync(model.Email);
            var token = await this.leaveUserManager.GeneratePasswordResetTokenAsync(user.Id);
            var callbackUrl = this.Url.Action("ResetPassword", "Home", new { userId = user.Id, token }, protocol: this.Request.Url?.Scheme);
            await this.leaveUserManager.SendEmailAsync(user.Id, "Reset password",
                "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");

            this.TempData["SuccessMessage"] = "Email sent with reset password";
            return this.RedirectToAction("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(UserResetViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            var result = await this.leaveUserManager.ResetPasswordAsync(model.UserId, model.Token, model.Password);
            if (result == IdentityResult.Success)
            {
                this.TempData["SuccessMessage"] = "Password changed";
            }
            else
            {
                this.TempData["ErrorMessage"] = "Failed to change password: " + string.Join(", ", result.Errors);
            }

            return this.RedirectToAction("Login");
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}