﻿using System.Web.Mvc;

namespace VacationsSystem.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Unauthorized() => this.View();

        public ActionResult EmailConfirm(string errors)
        {
            this.ViewBag.Errors = errors;
            return this.View();
        }
    }
}