﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Nager.Date;
using Nager.Date.Extensions;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Extensions;

namespace VacationsSystem.Controllers
{
    [AllowAnonymous]
    public class JsonCheckController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public JsonCheckController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        public JsonResult AreDatesOk(DateTime? startDate, DateTime? endDate)
        {
            if (startDate != null && endDate != null)
            {
                return this.Json(startDate <= endDate);
            }
            return this.Json(true);
        }

        [HttpPost]
        public async Task<JsonResult> IsUserFree(string userName, string ustId, string email)
        {
            var user = await this.unitOfWork.UserRepository.Find(u => u.UserName == userName ||
                                                                u.UstId == ustId ||
                                                                u.Email == email);
            return this.Json(user.FirstOrDefault() == null);
        }

        [HttpPost]
        public JsonResult HowManyDays(DateTime start, DateTime end)
        {
            return this.Json(DateTools.HowManyDays(start, end));
        }

        [HttpPost]
        public JsonResult IsPublicHoliday(DateTime date)
        {
            return this.Json(DateSystem.IsPublicHoliday(date, CountryCode.PL));
        }

        [HttpPost]
        public async Task<JsonResult> IsEmailInUse(string email)
        {
            var user = await this.unitOfWork.UserRepository.Find(u => u.Email == email);
            return this.Json(user.FirstOrDefault() != null);
        }

        [HttpPost]
        [Authorize]
        public async Task<JsonResult> EnoughDays(int daysTaken)
        {
            var userId = this.User.Identity.GetUserId();
            var user = await this.unitOfWork.UserRepository.Get(userId);
            return this.Json(user.VacationDays >= daysTaken);
        }
    }
}