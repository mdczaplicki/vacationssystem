﻿using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using VacationsSystem.Attributes;
using VacationsSystem.DAL;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Extensions;
using VacationsSystem.Models;
using VacationsSystem.Services;
using VacationsSystem.ViewModels;
using X.PagedList.Mvc;
using X.PagedList.Mvc.Common;

namespace VacationsSystem.Controllers
{
    [AuthorizeRedirect(Roles = "Administrator")]
    public class UserController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserService userService;

        private readonly LeaveUserManager leaveUserManager;

        public UserController(LeaveUserManager userManager, IUnitOfWork unitOfWork)
        {
            this.leaveUserManager = userManager;
            this.unitOfWork = unitOfWork;
            this.userService = new UserService(unitOfWork);
        }
        public static readonly PagedListRenderOptions PagedListRenderOptions = new PagedListRenderOptions
        {
            LiElementClasses = new[] { "page-item" },
            PageClasses = new[] { "page-link" },
            DisplayLinkToPreviousPage = PagedListDisplayMode.Always,
            DisplayLinkToNextPage = PagedListDisplayMode.Always,
            UlElementClasses = new[] { "pagination", "justify-content-center" },
            LinkToPreviousPageFormat = "Previous",
            LinkToNextPageFormat = "Next"
        };

        #region HttpGet

        public async Task<ActionResult> Index(string sortOrder, ListSortDirection? orderDirection, 
            string filterName, string filterUstId, string filterSuperior, bool? filterIsEmployed, int? page)
        {
            sortOrder = sortOrder ?? "LastName";
            if (!typeof(User).HasProperty(sortOrder))
            {
                return this.RedirectToAction("Index");
            }
            orderDirection = orderDirection ?? ListSortDirection.Ascending;
            this.ViewData[sortOrder] = orderDirection ^ ListSortDirection.Descending;

            filterName = filterName?.ToLower() ?? "";
            filterUstId = filterUstId?.ToLower() ?? "";
            filterSuperior = filterSuperior?.ToLower() ?? "";
            filterIsEmployed = filterIsEmployed ?? true;
            page = page ?? 1;

            var users = this.userService.GetFilteredListOfUsers(filterName, filterUstId, filterSuperior,
                filterIsEmployed.Value, page.Value, sortOrder, orderDirection.Value);

            this.ViewBag.Page = page;
            this.ViewBag.SortOrder = sortOrder;
            this.ViewBag.FilterName = filterName;
            this.ViewBag.FilterUstId = filterUstId;
            this.ViewBag.FilterSuperior = filterSuperior;
            this.ViewBag.OrderDirection = orderDirection;
            this.ViewBag.FilterIsEmployed = filterIsEmployed;
            this.ViewBag.RenderOptions = PagedListRenderOptions;

            return this.View(await users);
        }

        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await this.unitOfWork.UserRepository.GetRecursive(u => u.Id == id, u => u.DirectSubordinates, null, u => u.Superior);
            if (user == null)
            {
                return this.HttpNotFound();
            }
            return this.View(user);
        }
        
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await this.unitOfWork.UserRepository.Get(id);
            if (user == null)
            {
                return this.HttpNotFound();
            }

            var userList = (await this.unitOfWork.UserRepository.Find(u => u.IsEmployed && u.Id != id)).Where(u =>
                    this.leaveUserManager.IsInRole(u.Id, "User")).OrderBy(u => u.LastName);
            this.ViewBag.SuperiorId = new SelectList(userList, "Id", "FullName", user.SuperiorId);
            return this.View(user);
        }
        
        public async Task<ActionResult> Assign(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await this.unitOfWork.UserRepository.Find(u => u.Id == id, includes: u => u.Roles);
            if (user.FirstOrDefault() == null)
            {
                return this.HttpNotFound();
            }

            var fullName = user.FirstOrDefault()?.FullName;
            var availableRoles = await this.unitOfWork.UserRoleRepository.GetAll(ur => ur.Name);
            
            var viewModel = new AssignRoleViewModel
            {
                FullName = fullName,
                AvailableRoles = availableRoles,
                InitialRoles = this.leaveUserManager.GetRoles(id).ToArray(),
                Id = id
            };
            this.TempData["UserId"] = id;
            return this.View(viewModel);
        }

        #endregion

        #region HttpPost

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(User model)
        {
            var isValid = true;
            var user = await this.unitOfWork.UserRepository.GetRecursive(model.Id, u => u.DirectSubordinates, null);
            if (user.AllSubordinates.Select(u => u.Id).Contains(model.SuperiorId))
            {
                this.ModelState.AddModelError("SuperiorId", Resources.Error.SubordinateAsSuperior);
                isValid = false;
            }

            if (!this.ModelState.IsValid || !isValid)
            {
                var userList = (await this.unitOfWork.UserRepository.Find(u => u.IsEmployed && u.Id != model.Id)).Where(u =>
                    this.leaveUserManager.IsInRole(u.Id, "User"));
                this.ViewBag.SuperiorId = new SelectList(userList, "Id", "FullName", user.SuperiorId);
                return this.View(model);
            }
            user.EditUser(model);
            await this.unitOfWork.Complete();

            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(string id)
        {
            var user = await this.unitOfWork.UserRepository.Get(u => u.Id == id, u => u.DirectSubordinates, u => u.Leaves);
            this.unitOfWork.UserRepository.Remove(user);
            await this.unitOfWork.Complete();
            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Assign(AssignRoleViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            this.leaveUserManager.RemoveFromRoles(model.Id, this.leaveUserManager.GetRoles(model.Id).ToArray());
            if (model.SelectedRoles.Any())
            {
                this.leaveUserManager.AddToRoles(model.Id, model.SelectedRoles.ToArray());
            }
            return this.RedirectToAction("Details", new { id = model.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AskConfirm(string id)
        {
            var token = await this.leaveUserManager.GenerateEmailConfirmationTokenAsync(id);
            var callbackUrl = this.Url.Action("ConfirmEmail", "Home", new { userId = id, token }, protocol: this.Request.Url?.Scheme);
            await this.leaveUserManager.SendEmailAsync(id, "Confirm email",
                "Please confirm your email by clicking <a href=\"" + callbackUrl + "\">here</a>");
            this.TempData["SuccessMessage"] = "Email sent.";
            return this.RedirectToAction("Index");
        }
        
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
