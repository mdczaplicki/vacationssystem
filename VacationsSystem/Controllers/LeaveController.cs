﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Nager.Date;
using VacationsSystem.Attributes;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Extensions;
using VacationsSystem.Models;
using VacationsSystem.Services;
using VacationsSystem.ViewModels;

namespace VacationsSystem.Controllers
{
    [AuthorizeRedirect(Roles = "User")]
    public class LeaveController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly LeaveService leaveService;
        private readonly UserService userService;

        public LeaveController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.userService = new UserService(unitOfWork);
            this.leaveService = new LeaveService(unitOfWork);
        }

        private static IEnumerable<string> GetCurrentHolidays(DateTime? start = null, DateTime? end = null)
        {
            return DateSystem.GetPublicHoliday(CountryCode.PL, start ?? DateTime.Now.AddMonths(-1), end ?? DateTime.Now.AddYears(1)).Select(h => h.Date.ToString("yyyy-MM-dd"));
        }

        #region HttpGet

        public async Task<ActionResult> Index(Leave.LeaveStateEnum? state)
        {
            state = state ?? Leave.LeaveStateEnum.All;
            var userId = this.User.Identity.GetUserId();
            var leaves = await this.unitOfWork.LeaveRepository.Find(
                l => (state.Value & (Leave.LeaveStateEnum) l.LeaveStateId) != 0 && l.UserId == userId,
                order: l => l.StartDate, includes: l => l.LeaveType);
            this.ViewBag.State = state;
            return this.View(leaves);
        }

        [AuthorizeRedirect(Roles = "Approver,Manager")]
        public async Task<ActionResult> OtherIndex(Leave.LeaveStateEnum? state, string sortOrder, ListSortDirection? orderDirection,
            string filterUser, [DataType(DataType.Date)] DateTime? filterStartDate, DateTime? filterEndDate, int? page)
        {
            filterStartDate = filterStartDate ?? DateTime.Now.Date.AddMonths(-1);
            filterEndDate = filterEndDate ?? DateTime.Now.Date.AddYears(1);
            state = state ?? Leave.LeaveStateEnum.Submitted;
            filterUser = filterUser?.ToLower() ?? "";
            sortOrder = sortOrder ?? "StartDate";
            page = page ?? 1;

            this.ViewBag.Holidays = GetCurrentHolidays(filterStartDate, filterEndDate);

            if (!typeof(Leave).HasProperty(sortOrder))
            {
                return this.RedirectToAction("OtherIndex");
            }

            orderDirection = orderDirection ?? ListSortDirection.Ascending;
            this.ViewData[sortOrder] = orderDirection ^ ListSortDirection.Descending;


            var loggedUserId = this.User.Identity.GetUserId();
            var leaves = await this.leaveService.GetLeavesOfAllSubordinatesAndSelfIncludingType(loggedUserId, (int) state.Value, filterUser,
                filterStartDate.Value, filterEndDate.Value, sortOrder, orderDirection.Value, page.Value);
            
            this.ViewBag.State = state;
            this.ViewBag.SortOrder = sortOrder;
            this.ViewBag.OrderDirection = orderDirection;
            this.ViewBag.FilterUser = filterUser;
            this.ViewBag.FilterStartDate = filterStartDate;
            this.ViewBag.FilterEndDate = filterEndDate;
            this.ViewBag.Page = page;
            this.ViewBag.RenderOptions = UserController.PagedListRenderOptions;

            return this.View(leaves);
        }

        [AuthorizeRedirect(Roles = "Administrator")]
        public async Task<ActionResult> AllLeaves()
        {
            var leaves = this.unitOfWork.LeaveRepository.GetAll(l => l.User, l => l.LeaveType);
            return this.View(await leaves);
        }

        [AuthorizeRedirect(Roles = "Administrator")]
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var leave = await this.unitOfWork.LeaveRepository.Get(id);
            if (leave == null)
            {
                return this.HttpNotFound();
            }

            this.ViewBag.Holidays = GetCurrentHolidays();
            var userList = await this.unitOfWork.UserRepository.GetAll();
            this.ViewBag.UserId = new SelectList(userList, "Id", "FullName", leave.UserId);
            return this.View(leave);
        }

        public async Task<ActionResult> Create(string id, bool forSelf = true)
        {
            this.ViewBag.Holidays = GetCurrentHolidays();

            if (id == null)
            {
                this.ViewBag.ForSelf = forSelf;
                if (!forSelf)
                {
                    var availableUsers = (await this.userService.GetAllSubordinatesAndSelf(this.User.Identity.GetUserId())).OrderBy(u => u.LastName).ToList();

                    this.ViewBag.UserSelectList = new SelectList(availableUsers, "Id", "FullName", this.User.Identity.GetUserId());
                }
                return this.View();
            }
            var leave = await this.unitOfWork.LeaveRepository.Get(id);
            if (leave == null || leave.UserId != this.User.Identity.GetUserId())
            {
                return this.HttpNotFound();
            }
            var model = new AddLeaveViewModel(leave);
            return this.View(model);
        }

        #endregion

        #region HttpPost

        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateConfirmed(AddLeaveViewModel model)
        {
            model.DaysTaken = DateTools.HowManyDays(model.StartDate, model.EndDate);
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            Leave leave;
            string message;
            var isForSelf = model.UserId.IsNullOrWhiteSpace();
            if (!model.LeaveId.IsNullOrWhiteSpace())
            {
                leave = await this.unitOfWork.LeaveRepository.Get(model.LeaveId);
                leave.UpdateLeave(model);
                message = "Leave edited";
            }
            else
            {
                leave = new Leave(model, this.User.Identity.GetUserId());
                if (!isForSelf)
                {
                    leave.LeaveStateId = (int) Leave.LeaveStateEnum.Submitted;
                }
                this.unitOfWork.LeaveRepository.Add(leave);
                message = "Leave created";
            }

            var userId = this.User.Identity.GetUserId();
            var user = await this.unitOfWork.UserRepository.Get(userId);
            if (user.VacationDays < leave.DaysTaken)
            {
                this.ModelState.AddModelError("DaysTaken", Resources.Error.NotEnoughDays);
                return this.View(model);
            }
            await this.unitOfWork.Complete();
            this.TempData["SuccessMessage"] = message;
            this.TempData["NewId"] = leave.Id;
            return isForSelf ? this.RedirectToAction("Index") : this.RedirectToAction("OtherIndex");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeRedirect(Roles = "Approver")]
        public async Task<ActionResult> Approve(string id)
        {
            var leave = await this.unitOfWork.LeaveRepository.Get(l => l.Id == id, includes: l => l.User);
            if (leave == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leave.LeaveStateId = (int)Leave.LeaveStateEnum.Approved;
            if (leave.User != null)
            {
                if (leave.User.VacationDays < leave.DaysTaken)
                {
                    this.TempData["BadId"] = id;
                    this.TempData["ErrorMessage"] = Resources.Error.NotEnoughDaysForUser;
                    return this.RedirectToAction("OtherIndex");
                }
                leave.User.VacationDays -= leave.DaysTaken ?? 0;
                if (leave.DaysTaken == null)
                {
                    this.TempData["ErrorMessage"] = Resources.Error.BadDaysTaken;
                }
            }
            await this.unitOfWork.Complete();
//            if (leave.User != null)
//            {
//                var approverObj = await this.unitOfWork.UserRepository.Get(this.User.Identity.GetUserId());
//                var approver = approverObj.FullName;
//                var user = leave.User.FullName;
//                var startDate = leave.StartDate?.ToShortDateString();
//                var endDate = leave.EndDate?.ToShortDateString();
//                await this.emailService.SendNotification(leave.User.Email, approverObj.Email, string.Format(Resources.Email.LeaveApproved, user),
//                    string.Format(Resources.Email.LeaveApprovedMessage, user, startDate, endDate, approver));
//            }
            this.TempData["NewId"] = id;
            this.TempData["SuccessMessage"] = "Leave approved";
            return this.RedirectToAction("OtherIndex");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Submit(string id)
        {
            var leave = await this.unitOfWork.LeaveRepository.Get(id);
            if (leave == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leave.LeaveStateId = (int)Leave.LeaveStateEnum.Submitted;
            await this.unitOfWork.Complete();
            this.TempData["NewId"] = id;
            this.TempData["SuccessMessage"] = "Leave submitted";
            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeRedirect(Roles = "Approver")]
        public async Task<ActionResult> Reject(string id)
        {
            var leave = await this.unitOfWork.LeaveRepository.Get(l => l.Id == id, includes: l => l.User);
            if (leave == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leave.LeaveStateId = (int)Leave.LeaveStateEnum.Rejected;
            await this.unitOfWork.Complete();
            this.TempData["NewId"] = id;
            this.TempData["SuccessMessage"] = "Leave rejected";
            return this.RedirectToAction("OtherIndex");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeRedirect(Roles = "Administrator")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StartDate,EndDate,Description,LeaveStateId,LeaveTypeId,UserId,DaysTaken")] Leave leave)
        {
            if (!this.ModelState.IsValid)
            {
                var userList = await this.unitOfWork.UserRepository.GetAll();
                this.ViewBag.UserId = new SelectList(userList, "Id", "FullName", leave.UserId);
                return this.View(leave);
            }
            this.unitOfWork.SetModified(leave);
            await this.unitOfWork.Complete();
            return this.RedirectToAction("AllLeaves");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(string id)
        {
            var leave = await this.unitOfWork.LeaveRepository.Get(id);
            if (leave == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            this.unitOfWork.LeaveRepository.Remove(leave);
            await this.unitOfWork.Complete();
            return this.RedirectToAction("AllLeaves");
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}