﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nager.Date;
using VacationsSystem.Attributes;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Extensions;
using VacationsSystem.Models;
using VacationsSystem.Services;
using VacationsSystem.ViewModels;

namespace VacationsSystem.Controllers
{
    [AuthorizeRedirect(Roles = "Hr")]
    public class HrController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserService userService;
        private readonly LeaveService leaveService;

        public HrController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.userService = new UserService(unitOfWork);
            this.leaveService = new LeaveService(unitOfWork);
        }

        #region HttpGet

        public async Task<ActionResult> UserList(string sortOrder, ListSortDirection? orderDirection,
            string filterName, bool? filterIsEmployed, int? page)
        {
            sortOrder = sortOrder ?? "LastName";
            if (!typeof(User).HasProperty(sortOrder))
            {
                return this.RedirectToAction("UserList");
            }
            orderDirection = orderDirection ?? ListSortDirection.Ascending;
            this.ViewData[sortOrder] = orderDirection ^ ListSortDirection.Descending;

            filterName = filterName?.ToLower() ?? "";
            filterIsEmployed = filterIsEmployed ?? true;
            page = page ?? 1;

            var users = await this.userService.GetFilteredListOfUsers(filterName, string.Empty, string.Empty,
                filterIsEmployed, page.Value, sortOrder, orderDirection.Value);

            this.ViewBag.FilterName = filterName;
            this.ViewBag.FilterIsEmployed = filterIsEmployed;
            this.ViewBag.SortOrder = sortOrder;
            this.ViewBag.OrderDirection = orderDirection;
            this.ViewBag.Page = page;
            this.ViewBag.RenderOptions = UserController.PagedListRenderOptions;

            return this.View(users);
        }
        
        public async Task<ActionResult> LeaveList(string sortOrder, ListSortDirection? orderDirection,
            string filterUser, [DataType(DataType.Date)] DateTime? filterStartDate, DateTime? filterEndDate, int? page, string submitButton)
        {
            if (submitButton == "Get xls file")
            {
                return await this.OnPostGetExcelFile(filterUser, filterStartDate, filterEndDate);
            }
            filterStartDate = filterStartDate ?? DateTime.Now.Date.AddMonths(-1);
            filterEndDate = filterEndDate ?? DateTime.Now.Date.AddYears(1);
            this.ViewBag.Holidays =
                DateSystem.GetPublicHoliday(CountryCode.PL, filterStartDate.Value, filterEndDate.Value).Select(h => h.Date.UnifiedFormat());

            sortOrder = sortOrder ?? "StartDate";
            if (!typeof(Leave).HasProperty(sortOrder))
            {
                return this.RedirectToAction("LeaveList");
            }

            orderDirection = orderDirection ?? ListSortDirection.Ascending;
            this.ViewData[sortOrder] = orderDirection ^ ListSortDirection.Descending;

            filterUser = filterUser?.ToLower() ?? "";
            page = page ?? 1;

            var leaves = await this.leaveService.GetFilteredListOfLeaves(filterUser, filterStartDate.Value, filterEndDate.Value, 
                (int) (Leave.LeaveTypeEnum.Abscence | Leave.LeaveTypeEnum.Maternal | Leave.LeaveTypeEnum.Vacation),
                (int) (Leave.LeaveStateEnum.Approved | Leave.LeaveStateEnum.ApprovedAndNotified),
                page.Value, sortOrder, orderDirection.Value);

            this.ViewBag.FilterUser = filterUser;
            this.ViewBag.FilterStartDate = filterStartDate;
            this.ViewBag.FilterEndDate = filterEndDate;
            this.ViewBag.SortOrder = sortOrder;
            this.ViewBag.OrderDirection = orderDirection;
            this.ViewBag.Page = page;
            this.ViewBag.RenderOptions = UserController.PagedListRenderOptions;

            return this.View(leaves);
        }

        public async Task<ActionResult> UserDetails(string userId)
        {
            var user = await this.unitOfWork.UserRepository.Get(u => u.Id == userId, includes: u => u.Leaves);
            var model = new UserDetailsViewModel(user);
            var leaveList = user.Leaves.OrderByDescending(u => u.StartDate).Take(5);
            this.unitOfWork.LeaveRepository.LoadMore(leaveList, l => l.LeaveState, l => l.LeaveType);
            this.ViewBag.LastLeaves = leaveList;

            return this.View(model);
        }

        #endregion

        #region HttpPost

//        [HttpPost]
//        [MultipleButton(Name = "action", Argument = "Filter")]
//        public async Task<ActionResult> Filter(string sortOrder, ListSortDirection? orderDirection,
//            string filterUser, [DataType(DataType.Date)] DateTime? filterStartDate, DateTime? filterEndDate, int? page)
//        {
//            return await this.LeaveList(sortOrder, orderDirection, filterUser, filterStartDate, filterEndDate, page);
//        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Notify(string id)
        {
            var leave = await this.unitOfWork.LeaveRepository.Get(l => l.Id == id, includes: l => l.User);
            if (leave == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leave.LeaveStateId = (int)Leave.LeaveStateEnum.ApprovedAndNotified;
            await this.unitOfWork.Complete();
            //            if (leave.User != null)
            //            {
            //                var approverObj = await this.unitOfWork.UserRepository.Get(this.User.Identity.GetUserId());
            //                var approver = approverObj.FullName;
            //                var user = leave.User.FullName;
            //                var startDate = leave.StartDate?.ToShortDateString();
            //                var endDate = leave.EndDate?.ToShortDateString();
            //                await this.emailService.SendNotification(leave.User.Email, approverObj.Email, string.Format(Resources.Email.LeaveApproved, user),
            //                    string.Format(Resources.Email.LeaveApprovedMessage, user, startDate, endDate, approver));
            //            }
            this.TempData["NewId"] = id;
            this.TempData["SuccessMessage"] = "Leave notified";
            return this.RedirectToAction("LeaveList");
        }

        public async Task<ActionResult> OnPostGetExcelFile(string filterUser,
            [DataType(DataType.Date)] DateTime? filterStartDate, [DataType(DataType.Date)] DateTime? filterEndDate)
        {
            const string fileName = "urlopy.xlsx";
            filterStartDate = filterStartDate ?? DateTime.Now.Date.AddMonths(-1);
            filterEndDate = filterEndDate ?? DateTime.Now.Date.AddYears(1);

            var fileStream = new MemoryStream();
            (await this.leaveService.GetExcelPackage(filterUser, filterStartDate.Value, filterEndDate.Value,
                (int) (Leave.LeaveTypeEnum.Abscence | Leave.LeaveTypeEnum.Maternal | Leave.LeaveTypeEnum.Vacation),
                (int) (Leave.LeaveStateEnum.Approved | Leave.LeaveStateEnum.ApprovedAndNotified), true)).SaveAs(fileStream);
            fileStream.Position = 0;
            var fsr = new FileStreamResult(fileStream,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = fileName
            };
            return fsr;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UserDetails(UserDetailsViewModel model)
        {
            var user = await this.unitOfWork.UserRepository.Get(model.UserId);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            user.VacationDays = model.VacationDays;
            user.VacationDaysDelta = model.VacationDaysDelta;
            user.Comment = model.Comment;

            await this.unitOfWork.Complete();

            this.TempData["SuccessMessage"] = "Values changed";

            return this.RedirectToAction("UserDetails", new { userId = model.UserId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeEmployedStatus(string userId)
        {
            var user = await this.unitOfWork.UserRepository.Get(userId);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            user.IsEmployed = !user.IsEmployed;
            await this.unitOfWork.Complete();
            return this.RedirectToAction("UserList");
        }

        #endregion

    }
}