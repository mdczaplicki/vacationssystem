﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using VacationsSystem.Attributes;
using VacationsSystem.DAL.UnitOfWork;
using VacationsSystem.Models;

namespace VacationsSystem.Controllers
{
    [AuthorizeRedirect(Roles = "Administrator")]
    public class UserRoleController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public UserRoleController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        
        public async Task<ActionResult> Index()
        {
            return this.View(await this.unitOfWork.UserRoleRepository.GetAll());
        }
        
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userRole = await this.unitOfWork.UserRoleRepository.Get(id);
            if (userRole == null)
            {
                return this.HttpNotFound();
            }
            userRole.CustomUsers = await this.unitOfWork.UserRepository.Find(u => u.Roles.Any(r => r.RoleId == userRole.Id));
            return this.View(userRole);
        }
        
        public ActionResult Create()
        {
            return this.View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create([Bind(Include = "Name", Exclude = "Id")] UserRole userRole)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(userRole);
            }
            this.unitOfWork.UserRoleRepository.Add(userRole);
            await this.unitOfWork.Complete();
            return this.RedirectToAction("Index");
        }
        
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userRole = this.unitOfWork.UserRoleRepository.Get(id);
            if (await userRole == null)
            {
                return this.HttpNotFound();
            }
            return this.View(await userRole);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] UserRole userRole)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(userRole);
            }
            this.unitOfWork.SetModified(userRole);
            await this.unitOfWork.Complete();
            return this.RedirectToAction("Index");
        }
        
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userRole = this.unitOfWork.UserRoleRepository.Get(id);
            if (await userRole == null)
            {
                return this.HttpNotFound();
            }
            return this.View(await userRole);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var userRole = this.unitOfWork.UserRoleRepository.Get(id);
            this.unitOfWork.UserRoleRepository.Remove(await userRole);
            await this.unitOfWork.Complete();
            return this.RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
