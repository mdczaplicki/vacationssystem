﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VacationsSystem.Models
{
    public class UserRole : IdentityRole
    {
        public UserRole() { }

        public UserRole(string name) : base(name) { }

        [NotMapped]
        public ICollection<User> CustomUsers { get; set; }
    }
}