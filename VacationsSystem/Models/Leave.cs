﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.Ajax.Utilities;
using VacationsSystem.ViewModels;

namespace VacationsSystem.Models
{
    public sealed class Leave
    {
        public enum LeaveTypeEnum
        {
            None = 0,
            Vacation = 1 << 0,
            Sick = 1 << 1,
            Abscence = 1 << 2,
            Maternal = 1 << 3,
            Other = 1 << 4
        }

        private static readonly Dictionary<LeaveTypeEnum, string> TypeNames = new Dictionary<LeaveTypeEnum, string>
        {
                { LeaveTypeEnum.Maternal, Resources.ExcelTypeNames.Maternal },
                { LeaveTypeEnum.Abscence, Resources.ExcelTypeNames.Abscence },
                { LeaveTypeEnum.Vacation, Resources.ExcelTypeNames.Vacation }
        };
        
        public enum LeaveStateEnum
        {
            None = 0,
            Created = 1 << 0,
            Submitted = 1 << 1,
            Approved = 1 << 2,
            Rejected = 1 << 3,
            [Display(Name = "Notified")]
            ApprovedAndNotified = 1 << 4,
            All = Created | Submitted | Approved | Rejected | ApprovedAndNotified
        }

        #region Contructors

        public Leave()
        { }

        public Leave(AddLeaveViewModel model, string creatorId)
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreatorId = creatorId;
            this.LeaveTypeId = model.LeaveTypeId;
            this.StartDate = model.StartDate;
            this.EndDate = model.EndDate;
            this.Description = model.Description;
            this.LeaveStateId = (int) LeaveStateEnum.Created;
            this.DaysTaken = model.DaysTaken;
            this.UserId = !model.UserId.IsNullOrWhiteSpace() ? model.UserId : creatorId;
        }

        #endregion

        #region Properties

        [Key]
        public string Id { get; set; }

        [CanBeNull]
        [ForeignKey("User")]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [CanBeNull]
        public User User { get; set; }
        
        [ForeignKey("LeaveType")]
        [Display(Name = "Leave type")]
        public int? LeaveTypeId { get; set; }
        
        [CanBeNull]
        [Display(Name = "Type")]
        public LeaveType LeaveType { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Start date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "End date")]
        public DateTime? EndDate { get; set; }

        [StringLength(1000)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [ForeignKey("LeaveState")]
        [Display(Name = "Leave state")]
        public int? LeaveStateId { get; set; }

        [Display(Name = "State")]
        public LeaveState LeaveState { get; set; }

        [Display(Name = "Days taken")]
        public int? DaysTaken { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreationDate { get; set; }

        [CanBeNull]
        [ForeignKey("User")]
        [Display(Name = "Creator")]
        public string CreatorId { get; set; }

        [CanBeNull]
        public User Creator { get; set; }

        #endregion

        #region Public Methods

        public void UpdateLeave(AddLeaveViewModel model)
        {
            this.StartDate = model.StartDate;
            this.EndDate = model.EndDate;
            this.Description = model.Description;
            this.LeaveTypeId = model.LeaveTypeId;
            this.DaysTaken = model.DaysTaken;
        }

        public string GetTypeName()
        {
            return TypeNames[LeaveType];
        }

        #endregion
    }
}