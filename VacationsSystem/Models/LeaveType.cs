﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VacationsSystem.Extensions;

namespace VacationsSystem.Models
{
    public class LeaveType
    {
        private LeaveType(Leave.LeaveTypeEnum @enum)
        {
            this.LeaveTypeId = (int) @enum;
            this.Name = @enum.ToString();
            this.Description = @enum.GetEnumDescription();
            this.Leaves = new ObservableCollection<Leave>();
        }

        protected LeaveType()
        {
            this.Leaves = new ObservableCollection<Leave>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LeaveTypeId { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Description { get; set; }

        [NotMapped]
        public ObservableCollection<Leave> Leaves { get; set; }

        public static implicit operator LeaveType(Leave.LeaveTypeEnum @enum) => new LeaveType(@enum);

        public static implicit operator Leave.LeaveTypeEnum(LeaveType type) => (Leave.LeaveTypeEnum) type.LeaveTypeId;
    }
}