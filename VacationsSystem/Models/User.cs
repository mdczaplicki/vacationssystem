﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.AspNet.Identity.EntityFramework;
using VacationsSystem.Extensions;
using VacationsSystem.ViewModels;

namespace VacationsSystem.Models
{
    public class User : IdentityUser
    {
        #region Constructors

        public User()
        {
            this.DirectSubordinates = new ObservableCollection<User>();
            this.Leaves = new ObservableCollection<Leave>();
        }

        public User(UserRegisterViewModel model)
        {
            this.FirstName = model.FirstName;
            this.LastName = model.LastName;
            this.PrivateEmail = model.PrivateEmail;
            this.UstId = model.UstId ?? (model.Email.IsHkEmail() ? "HK" + model.LastName : "U" + model.LastName);
            this.IsEmployed = true;
            this.UserName = model.UserName;
            this.Email = model.Email;
            this.VacationDaysDelta = 20;
        }

        public void EditUser(User model)
        {
            this.FirstName = model.FirstName;
            this.LastName = model.LastName;
            this.PrivateEmail = model.PrivateEmail;
            this.Email = model.Email;
            this.VacationDays = model.VacationDays;
            this.VacationDaysDelta = model.VacationDaysDelta;
            this.UserName = model.UserName;
            this.UstId = model.UstId;
            this.IsEmployed = model.IsEmployed;
            this.SuperiorId = model.IsEmployed ? model.SuperiorId : null;
            this.Comment = model.Comment;
        }

        #endregion

        #region Properties

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        
        [EmailAddress]
        [Display(Name = "Private email")]
//TODO:        [DisplayFormat(NullDisplayText = "No private email")]
        public string PrivateEmail { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "UST email")]
        [StringLength(100)]
        public sealed override string Email { get; set; }
        
        [Index(IsUnique = true)]
        [StringLength(100)]
        [Display(Name = "Ust ID")]
        public string UstId { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [Display(Name = "User name")]
        public sealed override string UserName { get; set; }

        [Required]
        [Display(Name = "Is employed")]
        public bool IsEmployed { get; set; }

        [CanBeNull]
        [ForeignKey("Superior")]
        [Display(Name = "Superior")]
        public string SuperiorId { get; set; }

        [CanBeNull]
        [DisplayFormat(NullDisplayText = "No superior")]
        [Display(Name = "Superior")]
        public User Superior{ get; set; }

        [DisplayFormat(NullDisplayText = "No phone number")]
        public override string PhoneNumber { get; set; }

        [Display(Name = "Direct subordinates")]
        public ObservableCollection<User> DirectSubordinates { get; set; }

        public ObservableCollection<Leave> Leaves { get; }

        [DefaultValue(0)]
        [Display(Name = "Vacation days")]
        public int? VacationDays { get; set; }

        [Required]
        [Display(Name = "Increment days by")]
        public int VacationDaysDelta { get; set; }

        [Display(Name = "Comment")]
        [StringLength(500)]
        public string Comment { get; set; }

        [NotMapped]
        [Display(Name = "Full name")]
        public string FullName => this.LastName + " " + this.FirstName;

        [NotMapped]
        [Display(Name = "All subordinates")]
        public IEnumerable<User> AllSubordinates => this.DirectSubordinates.Map(u => true, u => u.DirectSubordinates);

        [NotMapped]
        public IEnumerable<User> AllSuperiors
        {
            get
            {
                ICollection<User> list = new List<User>();
                var superior = this.Superior;
                while (superior != null)
                {
                    list.Add(superior);
                    superior = superior.Superior;
                }
                return list;
            } 
        }
        
        #endregion
    }
}