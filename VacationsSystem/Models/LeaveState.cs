﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VacationsSystem.Extensions;

namespace VacationsSystem.Models
{
    public class LeaveState
    {
        private LeaveState(Leave.LeaveStateEnum @enum)
        {
            this.LeaveStateId = (int)@enum;
            this.Name = @enum.ToString();
            this.Description = @enum.GetEnumDescription();
            this.Leaves = new ObservableCollection<Leave>();
        }

        protected LeaveState()
        {
            this.Leaves = new ObservableCollection<Leave>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LeaveStateId { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Description { get; set; }

        [NotMapped]
        public ObservableCollection<Leave> Leaves { get; set; }

        public static implicit operator LeaveState(Leave.LeaveStateEnum @enum) => new LeaveState(@enum);

        public static implicit operator Leave.LeaveStateEnum(LeaveState type) => (Leave.LeaveStateEnum)type.LeaveStateId;
    }
}